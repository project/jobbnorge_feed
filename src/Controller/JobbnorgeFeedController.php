<?php

namespace Drupal\jobbnorge_feed\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\jobbnorge_feed\FeedManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Jobbnorge feed routes.
 */
class JobbnorgeFeedController extends ControllerBase {

  /**
   * This manager here.
   *
   * @var \Drupal\jobbnorge_feed\FeedManager
   */
  protected $feedManager;

  /**
   * Constructor.
   */
  public function __construct(FeedManager $feedManager) {
    $this->feedManager = $feedManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('jobbnorge_feed.feed_manager')
    );
  }

  /**
   * Builds the response.
   */
  public function build($id, $trid) {
    $feed = $this->feedManager->getFeed($id, $trid);
    $render_items = [];
    foreach ($feed->getItems() as $item) {
      $render_items[] = [
        '#theme' => 'jobbnorge_feed_item',
        '#item' => $item,
        '#title' => $item->getTitle(),
        '#body' => $item->getDescription(),
        '#application_deadline' => $item->getDeadlineText(),
        '#location' => $item->getLocation(),
      ];
    }
    return [
      '#cache' => [
        'contexts' => [
          'url',
        ],
        'max-age' => 0,
      ],
      '#theme' => 'jobbnorge_feed',
      '#items' => $render_items,
    ];
  }

}
