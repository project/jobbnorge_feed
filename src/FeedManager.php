<?php

namespace Drupal\jobbnorge_feed;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactory;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * FeedManager service managing feeds.
 */
class FeedManager {

  const CONFIG_KEY = 'default_feed_id';
  const CACHE_PREFIX = 'jobbnorge_feed_feed_cache';
  const CACHE_TAG_PREFIX = 'jobbnorge_feed_feed';

  /**
   * Base Url.
   *
   * @var string
   */
  private $baseUrl;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a FeedManager object.
   */
  public function __construct(ClientInterface $http_client, CacheBackendInterface $cacheBackend, EventDispatcherInterface $eventDispatcher, ConfigFactory $configFactory) {
    $this->httpClient = $http_client;
    $this->cache = $cacheBackend;
    $this->eventDispatcher = $eventDispatcher;
    $this->config = $configFactory->get('jobbnorge_feed.settings');
  }

  /**
   * Update the site wide default feed.
   *
   * This is so you can set one feed site-wide, and create many block instances
   * of it, for example.
   */
  public function updateDefaultFeed() {
    return $this->updateFeed($this->getDefaultFeedId(), NULL);
  }

  /**
   * Update the feed.
   */
  public function updateFeed($feed_id, $tr_id) {
    $data = $this->getResponse($feed_id, $tr_id);
    return $data;
  }

  /**
   * Get the site wide default feed.
   *
   * Uses cache when relevant and possible.
   */
  public function getDefaultFeed() {
    return $this->getFeed($this->getDefaultFeedId());
  }

  /**
   * Get the feed in question.
   */
  public function getFeed($feed_id, $tr_id = NULL) : JobbNorgeResult {
    $cid = self::getCacheId($feed_id, $tr_id);
    $data = &$this->wrapDrupalStatic($cid);
    if ($data) {
      return $data;
    }
    // Consult the database cache.
    $cache_data = $this->cache->get($cid);
    if ($cache_data) {
      $data = $cache_data->data;
      return $data;
    }
    $data = $this->updateFeed($feed_id, $tr_id);
    $this->cache->set($cid, $data, Cache::PERMANENT, [
      self::getCacheTag($feed_id, $tr_id),
    ]);
    return $data;
  }

  /**
   * Wrap the method drupal_static so we can easier unit test it.
   */
  protected function &wrapDrupalStatic($cid) {
    $data = &drupal_static($cid);
    return $data;
  }

  /**
   * Helper for the cid.
   */
  public static function getCacheId($feed_id, $tr_id = NULL) {
    return sprintf('%s:%s:%s', self::CACHE_PREFIX, $feed_id, $tr_id);
  }

  /**
   * Static helper for cache tags.
   */
  public static function getCacheTag($feed_id, $tr_id = NULL) : string {
    return sprintf('%s:%s:%s', self::CACHE_TAG_PREFIX, $feed_id, $tr_id);
  }

  /**
   * Setter for base url.
   */
  public function setBaseUrl(string $url) {
    $this->baseUrl = $url;
  }

  /**
   * Helper to get the default feed for the site.
   */
  protected function getDefaultFeedId() {
    return $this->config->get(self::CONFIG_KEY);
  }

  /**
   * Wrap the HTTP things.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function getResponse($feed_id, $tr_id) : JobbNorgeResult {
    $query = [
      'id' => $feed_id,
    ];
    if ($tr_id) {
      $query['trid'] = $tr_id;
      $query['langid'] = $tr_id;
    }
    $result = $this->httpClient->request('GET', $this->getUri(), [
      RequestOptions::QUERY => $query,
    ]);
    return JobbNorgeResult::createFromXmlString((string) $result->getBody());
  }

  /**
   * Helper.
   */
  protected function getUri() {
    return $this->baseUrl ?? 'https://www.jobbnorge.no/apps/joblist/JobListBuilder.ashx';
  }

}
