<?php

namespace Drupal\jobbnorge_feed\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Jobbnorge feed settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'jobbnorge_feed_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['jobbnorge_feed.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['default_feed_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default feed ID for the site'),
      '#default_value' => $this->config('jobbnorge_feed.settings')->get('default_feed_id'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('jobbnorge_feed.settings')
      ->set('default_feed_id', $form_state->getValue('default_feed_id'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
