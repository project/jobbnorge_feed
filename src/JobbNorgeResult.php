<?php

namespace Drupal\jobbnorge_feed;

use Symfony\Component\Serializer\Encoder\XmlEncoder;

/**
 * A value object.
 */
class JobbNorgeResult {

  /**
   * Items in the feed.
   *
   * @var array
   */
  private array $items = [];

  /**
   * Static create method.
   */
  public static function createFromXmlString(string $xml) {
    $instance = new static();
    // Parse the XML.
    $encoder = new XmlEncoder();
    $data = $encoder->decode($xml, 'xml', [
      'as_collection' => TRUE,
    ]);
    if (!empty($data["channel"][0]["item"][0]) && is_array($data["channel"][0]["item"][0])) {
      $instance->setItems(self::getItemsFromArray($data["channel"][0]["item"]));
    }
    return $instance;
  }

  /**
   * Helper for the items.
   *
   * @return \Drupal\jobbnorge_feed\Item[]
   *   An array of items.
   */
  protected static function getItemsFromArray(array $items) : array {
    return array_map(function ($item) {
      return Item::createFromArray($item);
    }, $items);
  }

  /**
   * Setter for items.
   */
  public function setItems(array $items) {
    $this->items = $items;
  }

  /**
   * Get all the items.
   *
   * @return \Drupal\jobbnorge_feed\Item[]
   *   All of the items in there. Or none for that matter.
   */
  public function getItems() : array {
    return $this->items;
  }

  /**
   * Are there even items?
   */
  public function hasItems() : bool {
    return (bool) count($this->getItems());
  }

}
