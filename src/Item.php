<?php

namespace Drupal\jobbnorge_feed;

/**
 * Value object for single item.
 */
class Item {

  /**
   * The title of the item.
   *
   * @var string
   */
  private $title = '';

  /**
   * The link to the job ad.
   *
   * @var string
   */
  private $link = '';

  /**
   * Reference code.
   *
   * @var string
   */
  private $referenceCode = '';

  /**
   * Employer name.
   *
   * @var string
   */
  private $employerName = '';

  /**
   * Position title.
   *
   * @var string
   */
  private $positionTitle = '';

  /**
   * Lead text.
   *
   * @var string
   */
  private $leadText = '';

  /**
   * Text.
   *
   * @var string
   */
  private $text = '';

  /**
   * Location.
   *
   * @var string
   */
  private $location = '';

  /**
   * Deadline text. Not an actual formatted date, I think.
   *
   * @var string
   */
  private $deadlineText = '';

  /**
   * A description. Usually a short one, good for teasers.
   *
   * @var string
   */
  private $description = '';

  /**
   * Department name.
   *
   * @var string
   */
  private $departmentName = '';

  /**
   * Top department name.
   *
   * @var string
   */
  private $topDepartmentName = '';

  /**
   * The department.
   *
   * @return string
   *   The department.
   */
  public function getDepartmentName(): string {
    return $this->departmentName;
  }

  /**
   * Setter.
   *
   * @param string $departmentName
   *   Dep name.
   */
  public function setDepartmentName(string $departmentName): void {
    $this->departmentName = $departmentName;
  }

  /**
   * Top department.
   *
   * @return string
   *   The top department.
   */
  public function getTopDepartmentName(): string {
    return $this->topDepartmentName;
  }

  /**
   * Setter.
   *
   * @param string $topDepartmentName
   *   Top dep name.
   */
  public function setTopDepartmentName(string $topDepartmentName): void {
    $this->topDepartmentName = $topDepartmentName;
  }

  /**
   * Getter.
   *
   * @return string
   *   The description.
   */
  public function getDescription() : string {
    return $this->description;
  }

  /**
   * Setter.
   *
   * @param string $description
   *   The description.
   */
  public function setDescription(string $description): void {
    $this->description = $description;
  }

  /**
   * Deadline text.
   *
   * @return string
   *   The deadline.
   */
  public function getDeadlineText() : string {
    return $this->deadlineText;
  }

  /**
   * Setter for deadline text.
   *
   * @param string $deadlineText
   *   Dead line text.
   */
  public function setDeadlineText(string $deadlineText): void {
    $this->deadlineText = $deadlineText;
  }

  /**
   * Getter for title.
   *
   * @return string
   *   The title.
   */
  public function getTitle(): string {
    return $this->title;
  }

  /**
   * Setter for title.
   *
   * @param string $title
   *   The title.
   */
  public function setTitle(string $title): void {
    $this->title = trim($title);
  }

  /**
   * Getter.
   *
   * @return string
   *   The link.
   */
  public function getLink(): string {
    return $this->link;
  }

  /**
   * Setter.
   *
   * @param string $link
   *   The link.
   */
  public function setLink(string $link): void {
    $this->link = $link;
  }

  /**
   * Getter.
   *
   * @return string
   *   The ref code.
   */
  public function getReferenceCode() : string {
    return $this->referenceCode;
  }

  /**
   * Setter.
   *
   * @param string $referenceCode
   *   The ref code.
   */
  public function setReferenceCode(string $referenceCode): void {
    $this->referenceCode = $referenceCode;
  }

  /**
   * Getter.
   *
   * @return string
   *   The name.
   */
  public function getEmployerName() : string {
    return $this->employerName;
  }

  /**
   * Setter.
   *
   * @param string $employerName
   *   The name.
   */
  public function setEmployerName(string $employerName): void {
    $this->employerName = $employerName;
  }

  /**
   * Getter.
   *
   * @return string
   *   The title.
   */
  public function getPositionTitle() : string {
    return $this->positionTitle;
  }

  /**
   * Setter.
   *
   * @param string $positionTitle
   *   The title.
   */
  public function setPositionTitle(string $positionTitle): void {
    $this->positionTitle = $positionTitle;
  }

  /**
   * Getter.
   *
   * @return string
   *   The text.
   */
  public function getLeadText() : string {
    return $this->leadText;
  }

  /**
   * Setter.
   *
   * @param string $leadText
   *   The text.
   */
  public function setLeadText(string $leadText): void {
    $this->leadText = $leadText;
  }

  /**
   * Getter.
   *
   * @return string
   *   The text.
   */
  public function getText() : string {
    return $this->text;
  }

  /**
   * Setter.
   *
   * @param string $text
   *   The text.
   */
  public function setText(string $text): void {
    $this->text = $text;
  }

  /**
   * Getter.
   *
   * @return string
   *   The location.
   */
  public function getLocation() : string {
    return $this->location;
  }

  /**
   * Setter.
   *
   * @param string $location
   *   The location I guess.
   */
  public function setLocation(string $location): void {
    $this->location = $location;
  }

  /**
   * Helper to create one.
   */
  public static function createFromArray(array $item) {
    $item_object = new static();

    $mappings = [
      'title' => [$item_object, 'setTitle'],
      'jn:leadtext' => [$item_object, 'setLeadText'],
      'jn:text' => [$item_object, 'setText'],
      'link' => [$item_object, 'setLink'],
      'jn:positiontitle' => [$item_object, 'setPositionTitle'],
      'jn:employername' => [$item_object, 'setEmployerName'],
      'jn:location' => [$item_object, 'setLocation'],
      'jn:refcode' => [$item_object, 'setReferenceCode'],
      'jn:deadline' => [$item_object, 'setDeadlineText'],
      'description' => [$item_object, 'setDescription'],
      'jn:departmentname' => [$item_object, 'setDepartmentName'],
      'jn:topdepartmentname' => [$item_object, 'setTopDepartmentName'],
    ];

    foreach ($mappings as $mapping => $call) {
      if (!empty($item[$mapping][0])) {
        try {
          call_user_func_array($call, [$item[$mapping][0]]);
        }
        catch (\Throwable $e) {
          // I guess that setting was not successful was it.
        }
      }
    }
    return $item_object;
  }

}
