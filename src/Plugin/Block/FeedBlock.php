<?php

namespace Drupal\jobbnorge_feed\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\jobbnorge_feed\FeedManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a jobb norge feed block.
 *
 * @Block(
 *   id = "jobbnorge_feed_feed_block",
 *   admin_label = @Translation("Jobb Norge Feed Block"),
 *   category = @Translation("Custom")
 * )
 */
class FeedBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The jobbnorge_feed.feed_manager service.
   *
   * @var \Drupal\jobbnorge_feed\FeedManager
   */
  protected $feedFeedManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new FeedBlock instance.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FeedManager $jobbnorge_feed_feed_manager, ConfigFactoryInterface $config_factory, AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->feedFeedManager = $jobbnorge_feed_feed_manager;
    $this->configFactory = $config_factory;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('jobbnorge_feed.feed_manager'),
      $container->get('config.factory'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'feed_id' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['feed_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Feed ID'),
      '#default_value' => $this->configuration['feed_id'],
      '#description' => $this->t('Enter a feed id to display in this block. Leave empty to use the site wide feed id, if set'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['feed_id'] = $form_state->getValue('feed_id');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $feed_id = $this->configFactory->get('jobbnorge_feed.settings')->get('default_feed_id');
    if (!empty($this->configuration['feed_id'])) {
      $feed_id = $this->configuration['feed_id'];
    }
    if (!$feed_id) {
      // Show something slightly more useful if you actually have the access to
      // something relevant.
      if ($this->currentUser->hasPermission('administer jobbnorge_feed configuration')) {
        return [
          '#markup' => 'This block has no Jobbnorge feed ID configured, and there is no default ID for the site',
          '#cache' => [
            'tags' => [
              'config.jobbnorge_feed.settings',
            ],
          ],
        ];
      }
    }
    $response = $this->feedFeedManager->getFeed($feed_id);
    if (!$response->hasItems()) {
      // Return a customizable message. Customizable in this case means being
      // able to override the template I guess.
      return [
        '#theme' => 'jobbnorge_feed_empty',
        '#feed_id' => $feed_id,
      ];
    }
    $render_items = [];
    foreach ($response->getItems() as $item) {
      $render_items[] = [
        '#theme' => 'jobbnorge_feed_item',
        '#item' => $item,
        '#title' => $item->getTitle(),
        '#application_deadline' => $item->getDeadlineText(),
        '#body' => $item->getLeadText(),
        '#location' => $item->getLocation(),
      ];
    }

    $build = [
      '#theme' => 'jobbnorge_feed',
      '#items' => $render_items,
    ];
    return $build;
  }

}
