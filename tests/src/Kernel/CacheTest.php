<?php

namespace Drupal\Tests\jobbnorge_feed\Kernel;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\jobbnorge_feed\FeedManager;
use Drupal\KernelTests\KernelTestBase;

/**
 * Test the feed manager.
 *
 * @group jobbnorge_feed
 */
class CacheTest extends KernelTestBase implements ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'jobbnorge_feed',
  ];

  /**
   * Test what happens when we fetch.
   */
  public function testCacheUsed() {
    /** @var \Drupal\Tests\jobbnorge_feed\Kernel\TestManager $mng */
    $mng = $this->container->get('jobbnorge_feed.feed_manager');
    // Initially, we are probably at 0 remote lookups, I hope.
    self::assertEquals(0, $mng->getRemoteCount());
    $mng->setBaseUrl('http://example.com');
    $feed_id = 'multi';
    $mng->getFeed($feed_id);
    self::assertEquals(1, $mng->getRemoteCount());
    // Now, next one should be the same one actually, totally statically cached.
    $mng->getFeed($feed_id);
    self::assertEquals(1, $mng->getRemoteCount());
    // Now let's reset the static cache, it should be cached in the database.
    drupal_static_reset(FeedManager::getCacheId($feed_id));
    $mng->getFeed($feed_id);
    self::assertEquals(1, $mng->getRemoteCount());
    // Then we can try to invalidate the cache tag. It should still use the
    // static cache (it was just set again).
    $this->container->get('cache_tags.invalidator')->invalidateTags([
      FeedManager::getCacheTag($feed_id),
    ]);
    $mng->getFeed($feed_id);
    self::assertEquals(1, $mng->getRemoteCount());
    // But if we now reset the static cache, it should go to the remote again.
    drupal_static_reset(FeedManager::getCacheId($feed_id));
    $mng->getFeed($feed_id);
    self::assertEquals(2, $mng->getRemoteCount());
  }

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $container->getDefinition('http_client')
      ->setClass(TestClient::class)
      ->setFactory(NULL);
    $container->getDefinition('jobbnorge_feed.feed_manager')
      ->setClass(TestManager::class);
  }

}
