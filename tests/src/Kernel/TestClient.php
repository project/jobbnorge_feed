<?php

namespace Drupal\Tests\jobbnorge_feed\Kernel;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

/**
 * Our own http client so we can intercept.
 */
class TestClient extends Client {

  /**
   * We are using this in the tests.
   */
  const TEST_URI = 'http://example.com';

  /**
   * {@inheritdoc}
   */
  public function request($method, $uri = '', array $options = []): ResponseInterface {
    if ($uri === self::TEST_URI) {
      $body = '';
      switch ($options[RequestOptions::QUERY]['id']) {
        case 'empty':
          $body = '<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xmlns:jn="https://export.jobbnorge.no/xml/">
  <channel>
    <title>Ledige stillinger</title>
    <link>https://www.jobbnorge.no</link>
    <description>Eksterne stillinger for example</description>
    <ttl>2</ttl>
  </channel>
</rss>';
          break;

        case 'single':
          $body = '<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xmlns:jn="https://export.jobbnorge.no/xml/">
  <channel>
    <title>Ledige stillinger</title>
    <link>https://www.jobbnorge.no</link>
    <description>Eksterne stillinger for example</description>
    <ttl>2</ttl>
    <item><title><![CDATA[ Administrativ leder ved Psykologisk institutt ]]> </title><link>https://www.jobbnorge.no/ledige-stillinger/stilling/1234/administrativ-leder-ved-psykologisk-institutt</link><guid isPermaLink="true">https://www.jobbnorge.no/ledige-stillinger/stilling/1234/administrativ-leder-ved-psykologisk-institutt</guid><description><![CDATA[<div class="department">Avdeling: <span>department</span></div><div class="language">Språk: <span>Bokmål</span></div><div class="deadline">Søknadsfrist: <span>7. oktober 2022</span></div>]]></description><pubDate>Fri, 16 Sep 2022 00:00:00 GMT</pubDate><jn:createdon>Fri, 09 Sep 2022 10:41:12 GMT</jn:createdon><jn:deadline>7. oktober 2022</jn:deadline><jn:title><![CDATA[Administrativ leder ved Psykologisk institutt]]></jn:title><jn:positiontitle><![CDATA[Kontorsjef]]></jn:positiontitle><jn:employername><![CDATA[example]]></jn:employername><jn:departmentname><![CDATA[department]]></jn:departmentname><jn:topdepartmentname><![CDATA[department]]></jn:topdepartmentname><jn:leadtext><![CDATA[<p>Some lead text markup</p>]]></jn:leadtext><jn:location><![CDATA[]]></jn:location><jn:text><![CDATA[<p>More markup</p>]]></jn:text><jn:refcode><![CDATA[2022/1234 ]]></jn:refcode><jn:jobscope><![CDATA[Heltid]]></jn:jobscope><jn:jobduration><![CDATA[Fast]]></jn:jobduration><jn:jobtype id="708">1054 Kontorsjef</jn:jobtype></item>
  </channel>
</rss>';
          break;

        case 'multi':
          $body = '<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xmlns:jn="https://export.jobbnorge.no/xml/">
  <channel>
    <title>Ledige stillinger</title>
    <link>https://www.jobbnorge.no</link>
    <description>Eksterne stillinger for example</description>
    <ttl>2</ttl>
    <item><title><![CDATA[ Administrativ leder ved Psykologisk institutt ]]> </title><link>https://www.jobbnorge.no/ledige-stillinger/stilling/1234/administrativ-leder-ved-psykologisk-institutt</link><guid isPermaLink="true">https://www.jobbnorge.no/ledige-stillinger/stilling/1234/administrativ-leder-ved-psykologisk-institutt</guid><description><![CDATA[<div class="department">Avdeling: <span>department</span></div><div class="language">Språk: <span>Bokmål</span></div><div class="deadline">Søknadsfrist: <span>7. oktober 2022</span></div>]]></description><pubDate>Fri, 16 Sep 2022 00:00:00 GMT</pubDate><jn:createdon>Fri, 09 Sep 2022 10:41:12 GMT</jn:createdon><jn:deadline>7. oktober 2022</jn:deadline><jn:title><![CDATA[Administrativ leder ved Psykologisk institutt]]></jn:title><jn:positiontitle><![CDATA[Kontorsjef]]></jn:positiontitle><jn:employername><![CDATA[example]]></jn:employername><jn:departmentname><![CDATA[department]]></jn:departmentname><jn:topdepartmentname><![CDATA[department]]></jn:topdepartmentname><jn:leadtext><![CDATA[<p>Some lead text markup</p>]]></jn:leadtext><jn:location><![CDATA[]]></jn:location><jn:text><![CDATA[<p>More markup</p>]]></jn:text><jn:refcode><![CDATA[2022/1234 ]]></jn:refcode><jn:jobscope><![CDATA[Heltid]]></jn:jobscope><jn:jobduration><![CDATA[Fast]]></jn:jobduration><jn:jobtype id="708">1054 Kontorsjef</jn:jobtype></item>
    <item><title><![CDATA[ Administrativ leder ved Psykologisk institutt ]]> </title><link>https://www.jobbnorge.no/ledige-stillinger/stilling/1234/administrativ-leder-ved-psykologisk-institutt</link><guid isPermaLink="true">https://www.jobbnorge.no/ledige-stillinger/stilling/1234/administrativ-leder-ved-psykologisk-institutt</guid><description><![CDATA[<div class="department">Avdeling: <span>department</span></div><div class="language">Språk: <span>Bokmål</span></div><div class="deadline">Søknadsfrist: <span>7. oktober 2022</span></div>]]></description><pubDate>Fri, 16 Sep 2022 00:00:00 GMT</pubDate><jn:createdon>Fri, 09 Sep 2022 10:41:12 GMT</jn:createdon><jn:deadline>7. oktober 2022</jn:deadline><jn:title><![CDATA[Administrativ leder ved Psykologisk institutt]]></jn:title><jn:positiontitle><![CDATA[Kontorsjef]]></jn:positiontitle><jn:employername><![CDATA[example]]></jn:employername><jn:departmentname><![CDATA[department]]></jn:departmentname><jn:topdepartmentname><![CDATA[department]]></jn:topdepartmentname><jn:leadtext><![CDATA[<p>Some lead text markup</p>]]></jn:leadtext><jn:location><![CDATA[]]></jn:location><jn:text><![CDATA[<p>More markup</p>]]></jn:text><jn:refcode><![CDATA[2022/1234 ]]></jn:refcode><jn:jobscope><![CDATA[Heltid]]></jn:jobscope><jn:jobduration><![CDATA[Fast]]></jn:jobduration><jn:jobtype id="708">1054 Kontorsjef</jn:jobtype></item>
  </channel>
</rss>';
          break;
      }

      $response = new Response(200, [], $body);
      return $response;
    }
    return parent::request($method, $uri, $options);
  }

}
