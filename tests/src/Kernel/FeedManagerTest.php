<?php

namespace Drupal\Tests\jobbnorge_feed\Kernel;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\jobbnorge_feed\FeedManager;
use Drupal\KernelTests\KernelTestBase;

/**
 * Test the feed manager.
 *
 * @group jobbnorge_feed
 */
class FeedManagerTest extends KernelTestBase implements ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'jobbnorge_feed',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() : void {
    parent::setUp();
    $this->installConfig(['jobbnorge_feed']);
  }

  /**
   * Test what happens when we fetch.
   *
   * @dataProvider getTypes
   */
  public function testFeedDataFetched($type) {
    $this->config('jobbnorge_feed.settings')
      // We use the type as the id, so we can return some dummy responses based
      // on that.
      ->set(FeedManager::CONFIG_KEY, $type)->save();
    /** @var \Drupal\jobbnorge_feed\FeedManager $mng */
    $mng = $this->container->get('jobbnorge_feed.feed_manager');
    $mng->setBaseUrl('http://example.com');
    $data = $mng->updateDefaultFeed();
    switch ($type) {
      case 'empty':
        self::assertEquals(0, count($data->getItems()));
        self::assertFalse($data->hasItems());
        break;

      case 'single':
        self::assertEquals(1, count($data->getItems()));
        self::assertTrue($data->hasItems());
        break;

      case 'multi':
        self::assertEquals(2, count($data->getItems()));
        self::assertTrue($data->hasItems());
        break;
    }
  }

  /**
   * A dataprovider.
   */
  public function getTypes() {
    return [
      ['empty'],
      ['single'],
      ['multi'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $container->getDefinition('http_client')
      ->setClass(TestClient::class)
      ->setFactory(NULL);
  }

}
