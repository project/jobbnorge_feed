<?php

namespace Drupal\Tests\jobbnorge_feed\Kernel;

use Drupal\jobbnorge_feed\FeedManager;
use Drupal\jobbnorge_feed\JobbNorgeResult;

/**
 * Our own manager so we can intercept.
 */
class TestManager extends FeedManager {

  /**
   * A counter for remote calls.
   *
   * @var int
   */
  private $remoteCount = 0;

  /**
   * {@inheritdoc}
   */
  protected function getResponse($feed_id, $tr_id): JobbNorgeResult {
    $this->remoteCount++;
    return parent::getResponse($feed_id, $tr_id);
  }

  /**
   * Helper to use for getting number of calls to remove servers and so on.
   */
  public function getRemoteCount() : int {
    return $this->remoteCount;
  }

}
