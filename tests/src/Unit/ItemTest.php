<?php

namespace Drupal\Tests\jobbnorge_feed\Unit;

use Drupal\jobbnorge_feed\JobbNorgeResult;
use Drupal\Tests\UnitTestCase;

/**
 * Test that value object.
 *
 * @group jobbnorge_feed
 */
class ItemTest extends UnitTestCase {

  /**
   * Test all the getters of our value object.
   *
   * @dataProvider getXmlVariations
   */
  public function testGetters($xml) {
    $result = JobbNorgeResult::createFromXmlString($xml);
    $getter_tests = [
      'getDepartmentName',
      'getTopDepartmentName',
      'getDescription',
      'getDeadlineText',
      'getTitle',
      'getLink',
      'getReferenceCode',
      'getEmployerName',
      'getPositionTitle',
      'getLeadText',
      'getText',
      'getLocation',
    ];
    foreach ($result->getItems() as $item) {
      foreach ($getter_tests as $getter_test) {
        $value = call_user_func([$item, $getter_test]);
        // We assert something to indicate this getter works.
        self::assertTrue(TRUE);
      }
    }
    self::assertTrue(TRUE, 'No PHP error was crashing this test before it finished yay');
  }

  /**
   * Our dataprovider for this.
   */
  public function getXmlVariations() {
    return [
      'empty_ref_code' => [
        'xml' => '<?xml version="1.0" encoding="utf-8"?><rss version="2.0" xmlns:jn="https://export.jobbnorge.no/xml/"><channel><title>Ledige stillinger</title><link>https://www.jobbnorge.no</link><description></description><ttl>2</ttl><item><jn:refcode><![CDATA[30180690]]></jn:refcode></item></channel></rss>',
      ],
      'empty_for_relevant_thing' => [
        'xml' => '<?xml version="1.0" encoding="utf-8"?><rss version="2.0" xmlns:jn="https://export.jobbnorge.no/xml/"><channel><title>Ledige stillinger</title><link>https://www.jobbnorge.no</link><description></description><ttl>2</ttl><item><scully>Mulder</scully></item></channel></rss>',
      ],
      'totally_empty_thing' => [
        'xml' => '<?xml version="1.0" encoding="utf-8"?><rss version="2.0" xmlns:jn="https://export.jobbnorge.no/xml/"><channel><title>Ledige stillinger</title><link>https://www.jobbnorge.no</link><description></description><ttl>2</ttl><item></item></channel></rss>',
      ],
    ];
  }

}
